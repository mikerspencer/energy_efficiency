# ---------------------------------------
# ---------------------------------------
# Clean electricty use data
# ---------------------------------------
# ---------------------------------------

source("functions.R")

f.ele = list.files("~/Cloud/Michael/Jobs/consulting/changeworks/exercise/data", pattern="ele", full.names=T)

f.ele = list.files("~/Documents/exercise/data", pattern="ele", full.names=T)


# ---------------------------------------
# Check file structure
dl.ele = lapply(f.ele, function(i){
   con = file(i, "rt")
   x = readLines(con, 5)
})

# ---------------------------------------
# Read files
dl.ele = lapply(f.ele, read.csv, sep="|", skip=4, header=F, strip.white=T, stringsAsFactors=F)


# ---------------------------------------
# Remove unwanted commas and extra columns

dl.ele[1] = lapply(dl.ele[1], function(i){
   i = i[, c(1, 2, 4:8)]
   i[, 4:7] = apply(i[, 4:7], 2, comma)
   i
})

dl.ele[2:3] = lapply(dl.ele[2:3], function(i){
   i = i[, c(1, 2, 4:6, 8:9)]
   i[, 4:7] = apply(i[, 4:7], 2, comma)
   i
})

dl.ele[4:5] = lapply(dl.ele[4:5], function(i){
   i = i[, c(1, 2, 6:8)]
   i[, 4:5] = apply(i[, 4:5], 2, comma)
   i
})


# ---------------------------------------
# Sum eco7 and standard
dl.ele[1:3] = lapply(dl.ele[1:3], function(i){
   i$kWh = i[, 4] + i[, 5]
   i$Meters = i[, 6] + i[, 7]
   i[, c(1:3, 8:9)]
})


# ---------------------------------------
# Column names, year and convert to dataframe
dl.ele = lapply(seq_along(dl.ele), function(i){
   colnames(dl.ele[[i]]) = c("LAname", "LAcode", "LSOAcode", "kWh.ele", "Meters.ele")
   dl.ele[[i]]$Year = c(2010:2014)[i]
   dl.ele[[i]]
})
df.ele = do.call("rbind.data.frame", dl.ele)


# ---------------------------------------
# Clean missing
df.ele = df.ele[complete.cases(df.ele), ]

df.ele = df.ele[nchar(df.ele$LSOAcode)==9, ]

# ---------------------------------------
# Write clean file
df.ele$kWh.Meter.ele = round(df.ele$kWh.ele / df.ele$Meters.ele)
write.csv(df.ele, "~/Documents/exercise/data/ele_clean.csv", row.names=F)
