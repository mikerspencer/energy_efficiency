## Readme file for Changeworks data analysis exercise

This repository contains the scripts to complete the Changeworks data analysis exercise.

They are:

* `clean_ele.R` cleans DECC electricity usage data
* `clean_gas.R` cleans DECC gas usage data
* `functions.R` custom functions for the exercise
* `efficiency.Rmd/html` analysis
